// To check if it is an iOS device
var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true: false);


// Helper function to parse query
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

// Get urls from parameters,
// seperates the data from the script's logic
var appUrl = getParameterByName('appUrl');
var failUrl = getParameterByName('failUrl');


if (iOS) {
    window.open(appUrl, "_self");
    //If the app is not installed the script will wait for 3sec and redirect to web.
    var loadedAt = +new Date();
    setTimeout(
        function(){
            if (+new Date() - loadedAt < 3000){
                window.location = failUrl;
            }
        }, 1000);
    
    // Open the direct address no matter,
    // the deeplink is successful or not, or cancel button pressed
    setTimeout(function() {
        window.location = failUrl;
    }, 1000);
} else {
    // Launch the website on Desktop browser
    window.location = failUrl;
}


// TODO: Android version
// if (Android) {...}